import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import Home from '../components/Home';
import Tasks from '../components/Tasks';
import TabBar from '../components/TabBar';

const Tab = createBottomTabNavigator()
const TabNavigator = () => {
    return (
    <Tab.Navigator tabBar={(props) => <TabBar {...props} />}>
        <Tab.Screen 
        name='Home' 
        component={Home} 
        initialParams={{ icon: 'home'}} 
        />
        <Tab.Screen
        name='Tasks' 
        component={Tasks}
        initialParams={{ icon: 'squared-plus'}}
        />
    </Tab.Navigator>
    );
};

export default TabNavigator;