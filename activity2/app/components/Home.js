import React, {useState} from 'react';
import { StyleSheet, View, FlatList } from 'react-native';
import AddTask from '../../components/addtask';
import TodoItem from '../../components/todoitem';
import Header from '../../components/header';


const Home = () => {
    const [task, setTask] = useState([]);

  const pressHandler = (key) => {
    setTask((prevTask) => {
      return prevTask.filter(task => task.key != key);
    })
  }

  const submitHandler = (text) => {
    setTask((prevTask) => {
      return [
        { text: text, key: Math.random().toString() },
        ...prevTask
      ];
    })
  }
    return (
        <View style={styles.container}>
      <Header />
      <View style={styles.content}>
        <AddTask submitHandler={submitHandler}/>
        <View style={styles.list}>
          <FlatList 
            data={task}
            renderItem={({ item }) => (
              <TodoItem item={item} pressHandler={pressHandler}/>
            )}
          />
        </View>
      </View>
    </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#76a6ef'
    },
    text: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#fff'
    },
    content: {
        padding: 40,
    },
    list: {
        marginTop: 20,
}
}); 

export default Home;