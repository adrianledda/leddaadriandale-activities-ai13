import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native'
import { Entypo } from '@expo/vector-icons';

const Tab = ({color, tab, onPress, icon}) => {
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
        {icon && <Entypo name={icon} size={20} color={color} />}
        <Text style={{ color }}>{tab.name}</Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 5,
    }
})

export default Tab;