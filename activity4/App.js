import React from 'react'
import { Text, View, SafeAreaView, TouchableOpacity } from 'react-native';
import ReasonImg from './assets/reason.svg'
import DemandImg from './assets/demand.svg'
import FlexibleImg from './assets/flexible.svg'
import FunImg from './assets/fun.svg'
import MoneyImg from './assets/money.svg'
import OnlineImg from './assets/online.svg'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen component={Home} name="Home" options={{headerShown:false}} />
        <Stack.Screen component={One} name="One" options={{headerShown:false}} />
        <Stack.Screen component={Two} name="Two" options={{headerShown:false}} />
        <Stack.Screen component={Three} name="Three" options={{headerShown:false}} />
        <Stack.Screen component={Four} name="Four" options={{headerShown:false}} />
        <Stack.Screen component={Five} name="Five" options={{headerShown:false}} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

const Home = ({navigation}) => {
  return (
    <SafeAreaView 
    style={{
      flex:1, 
      justifyContent:'center', 
      alignItems:'center', 
      backgroundColor:'#fff',
      }}>
      <View style={{marginTop:70}}>
      <Text style={{fontSize:30, fontWeight:'bold', color:'#20315f'}}>
        WHY I CHOSE THE IT COURSE
        </Text>
    </View>
    <View style={{flex:1, justifyContent:'center', alignItems:'center'}} />
    <ReasonImg width={390} height={300} />
    <TouchableOpacity
    onPress={() => navigation.navigate('One')} 
    style={{
      backgroundColor:'#AD40AF', 
      padding:20, width:'90%', 
      borderRadius:5,
      flexDirection: 'row',
      justifyContent:'center',
      marginBottom: 20,
      marginTop: 150
      }}>
      <Text style={{fontWeight:'bold', fontSize:18, color:'#fff'}}>Continue</Text>
    </TouchableOpacity>
    </SafeAreaView>
  );
};

const One = ({navigation}) => {
  return (
    <SafeAreaView 
    style={{
      flex:1, 
      justifyContent:'center', 
      alignItems:'center', 
      backgroundColor:'#fff',
      }}>
      <View style={{marginTop:70}}>
      <Text style={{fontSize:30, fontWeight:'bold', color:'#20315f'}}>
        IN GREAT DEMAND
        </Text>
    </View>
    <View style={{flex:1, justifyContent:'center', alignItems:'center'}} />
    <DemandImg width={390} height={300} />
    <TouchableOpacity
    onPress={() => navigation.navigate('Two')} 
    style={{
      backgroundColor:'#AD40AF', 
      padding:20, width:'90%', 
      borderRadius:5,
      flexDirection: 'row',
      justifyContent:'center',
      marginBottom: 20,
      marginTop: 150
      }}>
      <Text style={{fontWeight:'bold', fontSize:18, color:'#fff'}}>Continue</Text>
    </TouchableOpacity>
    </SafeAreaView>
  );
};

const Two = ({navigation}) => {
  return (
    <SafeAreaView 
    style={{
      flex:1, 
      justifyContent:'center', 
      alignItems:'center', 
      backgroundColor:'#fff',
      }}>
      <View style={{marginTop:70}}>
      <Text style={{fontSize:30, fontWeight:'bold', color:'#20315f'}}>
        FLEXIBLE WORK STYLE
        </Text>
    </View>
    <View style={{flex:1, justifyContent:'center', alignItems:'center'}} />
    <FlexibleImg width={390} height={300} />
    <TouchableOpacity
    onPress={() => navigation.navigate('Three')} 
    style={{
      backgroundColor:'#AD40AF', 
      padding:20, width:'90%', 
      borderRadius:5,
      flexDirection: 'row',
      justifyContent:'center',
      marginBottom: 20,
      marginTop: 150
      }}>
      <Text style={{fontWeight:'bold', fontSize:18, color:'#fff'}}>Continue</Text>
    </TouchableOpacity>
    </SafeAreaView>
  );
};

const Three = ({navigation}) => {
  return (
    <SafeAreaView 
    style={{
      flex:1, 
      justifyContent:'center', 
      alignItems:'center', 
      backgroundColor:'#fff',
      }}>
      <View style={{marginTop:70}}>
      <Text style={{fontSize:30, fontWeight:'bold', color:'#20315f'}}>
        FUN AND EXCITING
        </Text>
    </View>
    <View style={{flex:1, justifyContent:'center', alignItems:'center'}} />
    <FunImg width={390} height={300} />
    <TouchableOpacity
    onPress={() => navigation.navigate('Four')} 
    style={{
      backgroundColor:'#AD40AF', 
      padding:20, width:'90%', 
      borderRadius:5,
      flexDirection: 'row',
      justifyContent:'center',
      marginBottom: 20,
      marginTop: 150
      }}>
      <Text style={{fontWeight:'bold', fontSize:18, color:'#fff'}}>Continue</Text>
    </TouchableOpacity>
    </SafeAreaView>
  );
};

const Four = ({navigation}) => {
  return (
    <SafeAreaView 
    style={{
      flex:1, 
      justifyContent:'center', 
      alignItems:'center', 
      backgroundColor:'#fff',
      }}>
      <View style={{marginTop:70}}>
      <Text style={{fontSize:30, fontWeight:'bold', color:'#20315f'}}>
        IT PAYS WELL
        </Text>
    </View>
    <View style={{flex:1, justifyContent:'center', alignItems:'center'}} />
    <MoneyImg width={390} height={300} />
    <TouchableOpacity
    onPress={() => navigation.navigate('Five')} 
    style={{
      backgroundColor:'#AD40AF', 
      padding:20, width:'90%', 
      borderRadius:5,
      flexDirection: 'row',
      justifyContent:'center',
      marginBottom: 20,
      marginTop: 150
      }}>
      <Text style={{fontWeight:'bold', fontSize:18, color:'#fff'}}>Continue</Text>
    </TouchableOpacity>
    </SafeAreaView>
  );
};

const Five = ({navigation}) => {
  return (
    <SafeAreaView 
    style={{
      flex:1, 
      justifyContent:'center', 
      alignItems:'center', 
      backgroundColor:'#fff',
      }}>
      <View style={{marginTop:70}}>
      <Text style={{fontSize:30, fontWeight:'bold', color:'#20315f'}}>
        INNOVATIVE CAREER FIELD
        </Text>
    </View>
    <View style={{flex:1, justifyContent:'center', alignItems:'center'}} />
    <OnlineImg width={390} height={300} />
    <TouchableOpacity
    onPress={() => navigation.navigate('Home')} 
    style={{
      backgroundColor:'#AD40AF', 
      padding:20, width:'90%', 
      borderRadius:5,
      flexDirection: 'row',
      justifyContent:'center',
      marginBottom: 20,
      marginTop: 150
      }}>
      <Text style={{fontWeight:'bold', fontSize:18, color:'#fff'}}>Home</Text>
    </TouchableOpacity>
    </SafeAreaView>
  );
};

export default App;
