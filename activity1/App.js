import { StatusBar } from 'expo-status-bar';
import React, {useState} from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';

export default function App() {
  const [string, setString] = useState('');

  return (
    <View style={styles.container}>
      <Text>Enter any text in the text box:</Text>
      <TextInput 
        style={styles.input}
        placeholder='e.g. PogChamp'
        onChangeText={(val) => setString(val)}/>
      <View style={styles.buttonContainer}>
        <Button title = 'save' onPress={() => alert('You just typed: ' + string)}/>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  marginContainer: {
    marginTop: 40
  },
  buttonContainer: {
    marginTop: 10
  },
  input: {
    borderWidth: 2,
    borderColor: 'black',
    padding: 8,
    margin: 10,
    width: 200,
  }
});